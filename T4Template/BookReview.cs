using System;
using System.Data;

namespace Model
{
    /// <summary>
    /// 数据表实体类：BookReview 
    /// </summary>
    [Serializable()]
    public class BookReview
    {
       /// <summary>
       /// Id
       /// </summary>     
       public int Id{ get; set; }

       /// <summary>
       /// BookId
       /// </summary>     
       public int BookId{ get; set; }

       /// <summary>
       /// Content
       /// </summary>     
       public string Content{ get; set; }


    }
}

