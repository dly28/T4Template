using System;
using System.Data;

namespace Model
{
    /// <summary>
    /// 数据表实体类：Book 
    /// </summary>
    [Serializable()]
    public class Book
    {
       /// <summary>
       /// Id
       /// </summary>     
       public int Id{ get; set; }

       /// <summary>
       /// Name
       /// </summary>     
       public string Name{ get; set; }


    }
}

